import QtQuick 2.0
import Ubuntu.Components 0.1
import Ubuntu.Components.ListItems 0.1
import Ubuntu.Components.Popups 0.1
import QtQuick.LocalStorage 2.0 as Sql

MainView {
    //objectName for functional testing purposes (autopilot-qt5)
    objectName: "mainView"
    id:root
    applicationName: "gdrush-qml"
    automaticOrientation:true;
    width: units.gu(100);
    height: units.gu(80);
    property real margins: units.gu(2)
    property real buttonWidth: units.gu(17)


    Page {
        id:gDrush
        property string pageTitle: i18n.tr("gDrush")
        property string drushCommand : ""
        property string moduleYesNoDialogTitle: ""
        property string moduleYesNoDialogText: ""
        property bool commandRunning: false
        title:pageTitle

        Component {
             id: deleteDialog
             Dialog {
                 id: deleteDialogue
                 title: "Delete Site From List?"
                 text: "Are you sure you want to delete this site from the list ?, this will not effect the files of the site it will delete it from gDrush sites list"
                 Button {
                     text: "Yes"
                     color: "green"
                     onClicked: gDrush.deleteSite(sitesListModel.getPath(caller.siteIndex));
                 }
                 Button {
                     text: "No"
                     color: "red"
                     onClicked: PopupUtils.close(deleteDialogue)
                 }
             }
        }
        Component {
             id: moduleYesNoDialog
             Dialog {
                 id: moduleYesNoDialogue
                 title: gDrush.moduleYesNoDialogTitle
                 text: gDrush.moduleYesNoDialogText
                 Button {
                     text: "Yes"
                     color: "green"
                     onClicked: {
                         scriptLauncher.writeCommand("y")
                         PopupUtils.close(moduleYesNoDialogue)
                     }
                 }
                 Button {
                     text: "No"
                     color: "red"
                     onClicked: {
                         scriptLauncher.writeCommand("n")
                         PopupUtils.close(moduleYesNoDialogue)
                     }
                 }
             }
        }
        Component {
                id: addSite

                Dialog {
                    id: addSiteDialog
                    function validate(){
                        if(!gdrushHelper.checkDir(sitePath.text) || siteName.text == ""){
                            sitePath.color = "red"
                            siteAddButton.color = "gray"
                            return false
                        }else if(gdrushHelper.checkDir(sitePath.text) && siteName.text != ""){
                            sitePath.color = "green"
                            siteAddButton.color = "orange"
                            return true
                        }
                    }

                    Column {
                        spacing: units.gu(2)
                        id: containerLayout
                        anchors {
                            top: parent.top
                            margins: units.gu(2)
                        }


                        Header {
                            text: i18n.tr("Add site")
                            __foregroundColor:"#ffffff"
                        }
                        Row{
                            spacing: units.gu(1)
                            Label{
                                text:"Name"
                            }

                            TextField {
                                id: siteName
                                width: addSite.width
                                highlighted:true
                                text: ""
                                onTextChanged: validate();
                            }
                        }

                        Row{
                            spacing: units.gu(2)
                            Label{
                                text:"Path"
                            }
                            TextField {
                                id: sitePath
                                width: addSite.width
                                highlighted: true
                                text: "/"
                                onTextChanged: validate();
                            }
                        }

                        Button {
                            id: siteAddButton
                            width: parent.width
                            text: "Add"
                            color: "gray"
                            onClicked: {
                                if(validate()){
                                    gDrush.saveSite(siteName.text,sitePath.text)
                                    PopupUtils.close(addSiteDialog)
                                    caller.pop.autoClose = true;
                                }
                            }

                        }
                        Button {
                            id: siteAddCancleButton
                            text: "Cancel"
                            width: parent.width
                            color: "red"
                            onClicked: {
                                    PopupUtils.close(addSiteDialog)
                                    caller.pop.autoClose = true;
                            }
                        }


                    }
                }
        }
        Component {
            id: siteSelector
            Popover {
                id: popover
                autoClose : false
                Column {
                    anchors {
                        top: parent.top
                        left: parent.left
                        right: parent.right
                    }
                    height: gDrush.height / 2
                    Header {
                        id: header
                        text: i18n.tr("Select Site")
                        Button {
                            id: siteAddButton
                            anchors {
                                margins: units.gu(3)
                                right: parent.right
                                verticalCenter:parent.verticalCenter
                            }

                            width:units.gu(5)
                            height: units.gu(3)
                            text: "+"
                            color:  "orange"
                            property Popover pop: popover
                            onClicked: {
                               PopupUtils.open(addSite,siteAddButton)
                               pop.autoClose = false
                            }
                        }
                    }

                    ListView {
                        id:sitesListView
                        clip: true
                        width: parent.width
                        height: parent.height - header.height
                        model: sitesListModel
                        delegate: Standard {
                            text: name
                            Button {
                                anchors {
                                    margins: units.gu(3)
                                    verticalCenter: parent.verticalCenter
                                    right: parent.right
                                }
                                id:siteDeleteButton
                                width:units.gu(5)
                                property int siteIndex: index
                                height: units.gu(3)
                                text: "X"
                                onClicked: {
                                    PopupUtils.open(deleteDialog, siteDeleteButton)
                                }
                            }
                            onClicked: {
                                caller.siteIndex = index
                                gDrush.title = gDrush.pageTitle + " : " + sitesListModel.getName(index);
                                scriptLauncher.setWorkingDirectory(sitesListModel.getPath(index))
                                gDrush.launchScript("pm-info","")
                                hide()
                            }
                        }
                    }
                }
            }
        }
        ListModel {
            id: sitesListModel
            ListElement {
                name: "None"
                path: ""
            }

            function getName(idx) {
                return (idx >= 0 && idx < count) ? get(idx).name: ""
            }

            function getPath(idx) {
                return (idx >= 0 && idx < count) ? get(idx).path: ""
            }
        }
        ListModel {
            id: modulesListModel

            function getAttr(attr,idx) {
                return (idx >= 0 && idx < count && get(idx)[attr]) ? get(idx)[attr]: ""
            }

            function isEnabled(idx){
                if(idx >= 0 && idx < count){
                    var status = get(idx).status;
                    if(status === "enabled"){
                        return true;
                    }
                }
                return false
            }

            function getExtension(idx) {
                return (idx >= 0 && idx < count) ? get(idx).extension: ""
            }

            function getTitle(idx) {
                return (idx >= 0 && idx < count) ? get(idx).title: ""
            }

            function getDescription(idx) {
                return (idx >= 0 && idx < count) ? get(idx).description: ""
            }

            function getVersion(idx) {
                return (idx >= 0 && idx < count) ? get(idx).v: ""
            }
        }
        Column{
            spacing: units.gu(2)
            anchors.left: parent.left
            anchors.margins: units.gu(1)
            Rectangle{
                color: "transparent"
                width: root.buttonWidth
                height: units.gu(2)
            }
            Row{
                spacing: units.gu(2)
                Column{
                    spacing: units.gu(2)
                    Grid {
                            id:cacheGrid
                            spacing: units.gu(2)
                            columns: 1
                            Button{
                                id: selectSiteButton
                                objectName: "selectSiteButton"
                                property int siteIndex: 0
                                width: root.buttonWidth
                                text: "Select Site"
                                onClicked: {
                                    gDrush.loadSites();
                                    PopupUtils.open(siteSelector, selectSiteButton)
                                }
                                color:"white"
                            }
                            Rectangle{
                                color: "transparent"
                                Label{
                                    id:clearCache
                                    color: "#000000"
                                    text:"Clear Cache"
                                    fontSize: "large"
                                    property string buttonColor: "#B2B2B2"
                                    anchors.centerIn: parent
                                }
                                width: root.buttonWidth
                                height: units.gu(4)
                            }

                            Button{
                                objectName: "allCache"
                                id:allCache
                                text: "All"
                                onClicked:  gDrush.launchScript("cc","all");
                                color: clearCache.buttonColor
                                width: root.buttonWidth
                            }

                            Button{
                                objectName: "drushCache"
                                id:drushCache
                                text: "Drush"
                                onClicked:  gDrush.launchScript("cc","drush");
                                color: clearCache.buttonColor
                                width: root.buttonWidth
                            }

                            Button{
                                objectName: "themeRegCache"
                                id:themeRegCache
                                text: "Theme Registry"
                                onClicked:  gDrush.launchScript("cc","theme-registry");
                                color: clearCache.buttonColor
                                width: root.buttonWidth
                            }

                            Button{
                                objectName: "menuCache"
                                id:menuCache
                                text: "Menu"
                                onClicked:  gDrush.launchScript("cc","menu");
                                color: clearCache.buttonColor
                                width: root.buttonWidth
                            }

                            Button{
                                objectName: "cssJsCache"
                                id:cssJsCache
                                text: "Css & Js"
                                onClicked:  gDrush.launchScript("cc","css-js");
                                color: clearCache.buttonColor
                                width: root.buttonWidth
                            }

                            Button{
                                objectName: "blockCache"
                                id:blockCache
                                text: "Block"
                                onClicked:  gDrush.launchScript("cc","block");
                                color: clearCache.buttonColor
                                width: root.buttonWidth
                            }

                            Button{
                                objectName: "moduleListCache"
                                id:moduleListCache
                                text: "Module List"
                                onClicked:  gDrush.launchScript("cc","module-list");
                                color: clearCache.buttonColor
                                width: root.buttonWidth
                            }

                            Button{
                                objectName: "themeListCache"
                                id:themeListCache
                                text: "Theme List"
                                onClicked:  gDrush.launchScript("cc","theme-list");
                                color: clearCache.buttonColor
                                width: root.buttonWidth
                            }

                            Button{

                                objectName: "registryCache"
                                id:registryCache
                                text: "Registry"
                                onClicked:  gDrush.launchScript("cc","registry");
                                color: clearCache.buttonColor
                                width: root.buttonWidth
                            }
                    }
                }
                Column{
                    spacing: units.gu(2)
                    Grid {
                        spacing: units.gu(2)
                        columns: 3
                        Rectangle{
                            color: "transparent"
                            Label{
                                id:pm
                                color: "#000000"
                                text:"Modules"
                                fontSize: "large"
                                property string buttonColor: "#B2B2B2"
                                anchors.centerIn: parent
                            }
                            width: root.buttonWidth
                            height: units.gu(4)
                        }
                        Button{
                            objectName: "pmList"
                            id:pmList
                            text: "Refresh"
                            onClicked:  {
                                gDrush.launchScript("pm-info","");

                            }
                            color: clearCache.buttonColor
                            width: root.buttonWidth
                        }
                    }
                    ListView {
                        id:modulesListView
                        clip: true
                        width: root.width - cacheGrid.width - units.gu(5)
                        height: units.gu(46)
                        model: modulesListModel
                        delegate: Standard {
                            text: title
                            Row{
                                anchors {
                                    margins: units.gu(3)
                                    verticalCenter: parent.verticalCenter
                                    right: parent.right
                                }
                                spacing: units.gu(2)
                                Switch {
                                    id:enableDisableSwitch
                                    checked: {
                                        modulesListModel.isEnabled(index);
                                    }

                                    onClicked: {
                                        if(!enableDisableSwitch.checked){
                                            gDrush.moduleYesNoDialogTitle = "Disable Module"
                                            gDrush.launchScript("dis",modulesListModel.getExtension(index));
                                        }else{
                                            gDrush.moduleYesNoDialogTitle = "Enable Module"
                                            gDrush.launchScript("en",modulesListModel.getExtension(index));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Row{
                        spacing: units.gu(2)
                        /*TextArea{
                            id: outputText
                            width: (root.width - cacheGrid.width) / 2 - units.gu(3)
                            objectName: "outputText"
                            text:""
                        }*/
                        TextArea{
                            id: errorText
                            width: (root.width - cacheGrid.width) - units.gu(5)
                            objectName: "errorText"
                            text:""
                        }
                    }
                }
            }
        }

        ActivityIndicator {
            anchors.top: gDrush.top
            anchors.right: gDrush.right
            anchors.margins: units.gu(2);
            running: gDrush.commandRunning === true
        }

        Connections {
            target: scriptLauncher;
            onFinishedText: {
                //outputText.text = str;
                if(gDrush[gDrush.drushCommand + "_finished"]){
                    gDrush[gDrush.drushCommand + "_finished"](str);
                }
                gDrush.commandRunning = false;
            }
            onUpdateText: {
                //outputText.text = str;
                if(gDrush[gDrush.drushCommand + "_running"]){
                    gDrush[gDrush.drushCommand + "_running"](str);
                }
            }
            onErrorText:{
                gDrush.commandRunning = true
                errorText.text = err;
                if(gDrush[gDrush.drushCommand + "_error"]){
                    gDrush[gDrush.drushCommand + "_error"](str);
                }
            }
            onStarted:{
                gDrush.commandRunning = true
                if(gDrush[gDrush.drushCommand + "_started"]){
                    gDrush[gDrush.drushCommand + "_started"](str);
                }
            }
        }

        function loadSites() {
            var db = Sql.LocalStorage.openDatabaseSync("DrupalSites", "1.0", "gDrush Drupal Sites list", 1000000);

            db.transaction(
                function(tx) {
                    // Create the database if it doesn't already exist
                    tx.executeSql('CREATE TABLE IF NOT EXISTS sites(name TEXT, path TEXT)');
                    var rs = tx.executeSql('SELECT * FROM sites');
                    var r = ""
                    sitesListModel.clear();
                    for(var i = 0; i < rs.rows.length; i++) {
                        sitesListModel.append({"name": rs.rows.item(i).name, "path": rs.rows.item(i).path})
                    }
                }
            )
        }

        function saveSite(name,path) {
            var db = Sql.LocalStorage.openDatabaseSync("DrupalSites", "1.0", "gDrush Drupal Sites list", 1000000);

            db.transaction(
                function(tx) {
                    // Create the database if it doesn't already exist
                    tx.executeSql('CREATE TABLE IF NOT EXISTS sites(name TEXT, path TEXT)');

                    // Add (another) greeting row
                    tx.executeSql('INSERT INTO sites VALUES(?, ?)', [ name, path ]);

                    loadSites();
                }
            )
        }

        function deleteSite(path) {
            var db = Sql.LocalStorage.openDatabaseSync("DrupalSites", "1.0", "gDrush Drupal Sites list", 1000000);

            db.transaction(
                function(tx) {
                    tx.executeSql('DELETE FROM sites WHERE path = ?', [ path ]);
                    loadSites();
                }
            )
        }

        function handleYesNo(str){
            var yesNo = str.search(/y\/n/);
            if(yesNo !== -1){
                gDrush.moduleYesNoDialogText = str.replace("(y/n):","");
                PopupUtils.open(moduleYesNoDialog);
            }
        }

        function drush_en_running(str) {
            handleYesNo(str);
        }

        function drush_dis_running(str) {
            handleYesNo(str);
        }

        function drush_en_finished(str) {
            launchScript("pm-info","");
        }

        function drush_dis_finished(str) {
            launchScript("pm-info","");
        }

        function drush_pm_info_finished(str) {
            var modules = str.match(/(.*\s\n)+\n/g);
            var regex = "[\\s]+:[\\s]+(.*)";
            var attr = ["extension","title","description","version","date","status","path","requires","required_by"];
            var attrRegx = [];
            var i = 0;
            for(i = 0;i< attr.length;i++){
                attrRegx[attr[i]] = new RegExp(attr[i].replace("_"," ")+regex, "gi");
            }
            modulesListModel.clear();
            for(i = 0;i < modules.length;i++){
                var module = modules[i];
                attrRegx["extension"].lastIndex = 0;
                attrRegx["title"].lastIndex = 0;
                var extension = attrRegx["extension"].exec(module)[1];
                var title = attrRegx["title"].exec(module)[1];
                if(extension && title){
                    var mod={};
                    mod["extension"] = extension.trim();
                    mod["title"] = title.trim();
                    var j = 0;
                    for(j = 2;j< attr.length;j++){
                        mod[attr[j]] = "Uknown";
                        attrRegx[attr[j]].lastIndex = 0;
                        var prob = attrRegx[attr[j]].exec(module);

                        if(prob && prob.length > 1){
                            mod[attr[j]] = prob[1].trim();
                        }

                    }
                    modulesListModel.append(mod);
                }
            }
        }

        function launchScript(cmd, args) {
            gDrush.drushCommand = "drush_" + cmd.replace(/[\s-]/g,"_");
            scriptLauncher.launchScript("drush" + " " + cmd + " " + args);
        }
    }
}
