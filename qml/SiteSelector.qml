import QtQuick 2.0
import Ubuntu.Components 0.1
import Ubuntu.Components.ListItems 0.1
import Ubuntu.Components.Popups 0.1

Component {
    id: siteSelector
    Popover {
        Column {
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }
            height: page1.height
            Header {
                id: header
                text: i18n.tr("Select Site")

                Button {
                    anchors {
                        margins: units.gu(3)
                        right: parent.right
                        verticalCenter:parent.verticalCenter
                    }
                    width:units.gu(5)
                    height: units.gu(3)
                    text: "+"
                    color:  "orange"
                    onClicked: {
                        console.log(index)
                    }
                }
            }

            ListView {
                clip: true
                width: parent.width
                height: parent.height - header.height
                model: sitesListModel
                delegate: Standard {
                    text: name
                    control: Button {
                                anchors {
                                    margins: units.gu(1)
                                    right: parent.right
                                }
                                width:units.gu(5)
                                height: units.gu(3)
                                text: "X"
                                onClicked: {
                                    console.log(index)
                                }
                            }
                    onClicked: {
                        caller.siteIndex = index
                        page1.title = page1.pageTitle + " : " + sitesListModel.getName(index);
                        hide()
                    }
                }
            }
        }
    }
}
